# code-server

![Version: 0.1.1](https://img.shields.io/badge/Version-0.1.1-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 4.10.1](https://img.shields.io/badge/AppVersion-4.10.1-informational?style=flat-square)

## Installing the chart
To install the chart with the release name `my-release`
```
$ helm repo add alekc-charts https://charts.alekc.dev
$ helm install --name my-release alekc-charts/code-server
```

---

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| alekc | <alexander@chernov.it> | <https://blog.alekc.org> |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| annotations.deployment | object | `{}` | Deployment's annotations |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.maxReplicas | int | `100` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| deployment.strategy | object | `{"type":"RollingUpdate"}` | Deployment strategy |
| env | object | `{"full":[],"simple":{"PGID":"1000","PUID":"1000","TZ":"Europe/London"}}` | environment values |
| env.full | list | `[]` | full envs, they will be rendered as written here. Useful if you need to use secrets/cm |
| env.simple | object | `{"PGID":"1000","PUID":"1000","TZ":"Europe/London"}` | simple key: val based env vars |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"linuxserver/code-server"` |  |
| image.tag | string | `""` |  |
| imagePullSecrets | list | `[]` |  |
| ingress.annotations | object | `{}` |  |
| ingress.className | string | `""` |  |
| ingress.enabled | bool | `false` |  |
| ingress.hosts[0].host | string | `"chart-example.local"` |  |
| ingress.hosts[0].paths[0].path | string | `"/"` |  |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` |  |
| ingress.tls | list | `[]` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| persistence.accessMode | string | `"ReadWriteOnce"` |  |
| persistence.additionalMounts | list | `[]` |  |
| persistence.additionalVolumes | list | `[]` |  |
| persistence.enabled | bool | `true` |  |
| persistence.existingClaim | string | `""` |  |
| persistence.size | string | `"800Mi"` |  |
| persistence.storageClass | string | `""` |  |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| rbac.admin.enabled | bool | `false` | If set to true, will give admin rights to the cluster where code-server is running. Enable it only if you know what you are doing and your instance is protected by password / oauth |
| rbac.admin.roleName | string | `"cluster-admin"` | Pre existing admin role name. Normally you would have admin, cluster-admin, edit, and view  out of box |
| rbac.enabled | bool | `true` |  |
| replicaCount | int | `1` |  |
| resources | object | `{}` |  |
| securityContext | object | `{}` |  |
| service.port | int | `8443` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.create | bool | `true` |  |
| serviceAccount.name | string | `""` |  |
| tolerations | list | `[]` |  |
